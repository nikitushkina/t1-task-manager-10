package ru.t1.nikitushkina.tm.api;

import ru.t1.nikitushkina.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

}
