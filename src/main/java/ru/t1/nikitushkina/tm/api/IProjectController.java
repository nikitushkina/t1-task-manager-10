package ru.t1.nikitushkina.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

}
