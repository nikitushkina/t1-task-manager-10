package ru.t1.nikitushkina.tm.api;

import ru.t1.nikitushkina.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    void clear();

    Project add(Project project);

}
