package ru.t1.nikitushkina.tm.api;

import ru.t1.nikitushkina.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project create(String name, String description);

    void clear();

    Project add(Project project);

}
