package ru.t1.nikitushkina.tm.api;

public interface ITaskController {

    void createTask();

    void showTask();

    void clearTask();

}
