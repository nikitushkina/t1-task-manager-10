package ru.t1.nikitushkina.tm.api;

import ru.t1.nikitushkina.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    void clear();

    Task add(Task task);

}
