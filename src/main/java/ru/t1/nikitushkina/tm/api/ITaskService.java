package ru.t1.nikitushkina.tm.api;

import ru.t1.nikitushkina.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task create(String name, String description);

    void clear();

    Task add(Task task);

}
