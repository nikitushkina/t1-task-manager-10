package ru.t1.nikitushkina.tm.controller;

import ru.t1.nikitushkina.tm.api.ICommandController;
import ru.t1.nikitushkina.tm.api.ICommandService;
import ru.t1.nikitushkina.tm.model.Command;
import ru.t1.nikitushkina.tm.util.FormatUtil;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showExit() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

    @Override
    public void showErrorArgument() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported...");
        System.exit(1);
    }

    @Override
    public void showErrorCommand() {
        System.err.println("[ERROR]");
        System.err.println("This command is not supported...");
        System.out.println();
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Olga Nikitushkina");
        System.out.println("email: onikitushkina@t1-consulting.ru ");
    }

    @Override
    public void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final long availableProcessors = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final String freeMemoryFormat = FormatUtil.format(freeMemory);
        final long maxMemory = runtime.maxMemory();
        final boolean maxMemoryCheck = (maxMemory == Long.MAX_VALUE);
        final String maxMemoryFormat = (maxMemoryCheck ? "no limit" : FormatUtil.format(maxMemory));
        final long totalMemory = runtime.totalMemory();
        final String totalMemoryFormat = FormatUtil.format(totalMemory);
        final long usageMemory = totalMemory - freeMemory;
        final String usageMemoryFormat = FormatUtil.format(usageMemory);

        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Maximum memory: " + maxMemoryFormat);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Usage memory: " + usageMemoryFormat);
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.10.0");
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands) {
            System.out.println(command);
        }
    }

}
