package ru.t1.nikitushkina.tm.controller;

import ru.t1.nikitushkina.tm.api.ITaskController;
import ru.t1.nikitushkina.tm.api.ITaskService;
import ru.t1.nikitushkina.tm.model.Task;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASKS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void showTask() {
        System.out.println("[SHOW TASKS]");
        int index = 1;
        final List<Task> tasks = taskService.findAll();
        for (final Task task : tasks) {
            System.out.println(index + ". " + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTask() {
        System.out.println("[CLEAR PROJECTS]");
        taskService.clear();
        System.out.println("[OK]");
    }

}
