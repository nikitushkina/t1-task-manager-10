package ru.t1.nikitushkina.tm.repository;

import ru.t1.nikitushkina.tm.api.IProjectRepository;
import ru.t1.nikitushkina.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public Project add(final Project project) {
        projects.add(project);
        return project;
    }

}
