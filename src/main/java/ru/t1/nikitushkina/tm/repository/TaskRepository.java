package ru.t1.nikitushkina.tm.repository;

import ru.t1.nikitushkina.tm.api.ITaskRepository;
import ru.t1.nikitushkina.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public Task add(final Task task) {
        tasks.add(task);
        return task;
    }

}
